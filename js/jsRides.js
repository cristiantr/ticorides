
/**function getRides()	{
	var rides = getFromLocalStorage('rides');
    rides = insertToTable('rides', ride);
	renderTable('rides', rides);
}*/

'use strict';

function saveRide() {
	var ridename = document.getElementById('ridename').value;
	var start_from = document.getElementById('start_from').value;
	var end = document.getElementById('end').value;
	var description = document.getElementById('description').value;
	var departure = document.getElementById('departure').value;
	var arrival = document.getElementById('arrival').value;
	var day = document.getElementById('day').value;	
		const ride ={
			 ridename,
		     start_from,
		     end,
		     description,
		     departure,
		     arrival,
		     day
		     } 
    
	const rides = insertToTable('rides', ride);

	// render the books
	//renderTable('rides', rides);
}

function renderTable(tableName, tableData) {
	let table = jQuery(`#${tableName}_table`);	
	let rows = "";
	tableData.forEach((ride, index) => {
		let row = `<tr><td>${ride.ridename}</td><td>${ride.start_from}</td><td>${ride.end}</td><td>${ride.description}</td><td>${ride.departure}</td><td>${ride.arrival}</td><td>${ride.day}</td>`;
		row += `<td> <a onclick="editEntity(this)" data-id="${ride.id}" data-entity="${tableName}" class="link edit">Edit</a>  |  <a  onclick="deleteEntity(this);" data-id="${ride.id}" data-entity="${tableName}" class="link delete">Delete</a>  </td>`;
		rows += row + '</tr>';
	});
	table.html(rows);
}


function editEntity(element) {
	
	const dataObj = jQuery(element).data();
    editElement(dataObj.entity, dataObj.ridename);
    let carrera = searchFromTable(dataObj.entity, dataObj.id)
	
}


function deleteEntity(element) {
	
	const dataObj = jQuery(element).data();
	const newEntities = deleteFromTable(dataObj.entity, dataObj.id);
	renderTable(dataObj.entity, newEntities);
}


function saveRideOld() {
	var ridename = document.getElementById('ridename').value;
	var start_from = document.getElementById('start_from').value;
	var end = document.getElementById('end').value;
	var description = document.getElementById('description').value;
	var departure = document.getElementById('departure').value;
	var arrival = document.getElementById('arrival').value;
	var day = document.getElementById('day').value;	
		var ride ={
			 ridename,
		     start_from,
		     end,
		     description,
		     departure,
		     arrival,
		     day
		     } 

		       if(localStorage.getItem('rides'))
		       {
			      
			       var rides = getFromLocalStorage('rides'); 
				   rides.push(ride);
				  
				   if (saveToLocalStorage('rides', rides)) 
				      {
					   console.log('Ride saved');
				      }
			    } 
			    else
			    {
			    	  var rides =  new Array();
			    	  rides.push(ride);
			    	  if (saveToLocalStorage('rides', rides)) 
				      {
					   console.log('Fist Ride saved');
				      }

			    }  
	   
    }
function loadTableData(tableName) {
	renderTable(tableName, getTableData(tableName));
}       
	
	/**
 * Binds the different events to the different elements of the page
 */
function bindEvents() {
	 //document.getElementById('login-button').addEventListener('click', loginButtonHandler);
	 //document.getElementById('register-button').addEventListener('click', registerButtonHandler);
	 jQuery('#login-button').bind('click', loginButtonHandler);
	 jQuery('#ride-button').bind('click', rideButtonHandler);
	 
}

function loginButtonHandler(element) {
	validateUser();
}

function rideButtonHandler(element) {
	saveRide();
}


bindEvents();
   